angular.module('MyToDoListApp', [])
    .controller('ToDoListController', function ($scope) {
        var model = {
            user : "Tuấn Anh",
            jobs : {
                undone : [
                    {name  : "Gọi điện thoại", done : false},
                    {name  : "Nghe nhạc", done : false},
                    {name  : "Xem bóng đá", done : false},
                    {name  : "Chơi game", done : false},
                    {name  : "Học bài", done : false}
                ],
                inProgress : [
                    {name  : "Chơi dota 2", done : false},
                    {name  : "Đập cửa", done : false},
                    {name  : "Đá bóng", done : false}
                ],
                done : [
                    {name  : "Xong việc" , done : true}
                ]
            }
        };


        $scope.jobs = model.jobs;
        $scope.user = model.user;
        $scope.undone = model.jobs.undone;
        $scope.inProgress = model.jobs.inProgress;
        $scope.done = model.jobs.done;

        $scope.createNewTodo = function(val){
            if(val == 'undone'){
                $scope.undone.push({
                    name : $scope.newUndone,
                    done : false
                });
                $scope.newUndone = '';
            } else if(val == 'inProgress'){
                $scope.inProgress.push({
                    name : $scope.newInProgress,
                    done : false
                });
                $scope.newInProgress = '';
            } else {
                $scope.done.push({
                    name : $scope.newDone,
                    done : true
                });
                $scope.newDone = '';
            }
        };

        $scope.incompleteCount = function (str) {
            var count = 0;
            if(str == 'done'){
                angular.forEach($scope.jobs[str],function(item){
                    if(item.done) count++
                });
                return count
            }
            angular.forEach($scope.jobs[str],function(item){
                if(!item.done) count++
            });
            return count;
        };
        $scope.warningLevel = function (str) {
            return $scope.incompleteCount(str) < 3 ? "label-success" : "label-warning";
        };

        $scope.removeTodo = function(str,i){
            return $scope.jobs[str].splice(i,1);
        };
        
    });