var app = angular.module("sportsStore", ["cart", "ngRoute"]);

app.constant("activeClass", "btn-primary").constant("productListPageCount", 3);

app.config(function($routeProvider){
    $routeProvider.when("/checkout", {
        templateUrl: "/view/checkout.html"
    });
    $routeProvider.when("/products", {
        templateUrl: "/view/list.html"
    });
    $routeProvider.when("/placeorder",{
        templateUrl : "/view/order.html"
    });
    $routeProvider.when("/complete",{
        templateUrl : "/view/thanks.html",
        controller : "OrderController"
    })
    $routeProvider.otherwise({
        templateUrl: "/view/list.html"
    });
    
});
app.controller("SportController", function ($scope, $http, $filter, activeClass, productListPageCount,cart) {
    $http({
        url: "http://localhost:2403/product",
        method: "GET"
    }).then(function successCallBack(response) {
        $scope.products = response.data;

    }, function errorCallback(response) {
        $scope.error = response;
    });

    var selectedCategory;
    $scope.selectedPage = 1;
    $scope.pageSize = productListPageCount;

    $scope.selectCategory = function (newCategory) {
        selectedCategory = newCategory;
        $scope.selectedPage = 1;
    };

    $scope.selectPage = function (newPage) {
        $scope.selectedPage = newPage;
    };

    $scope.categoryFilterFn = function (product) {
        return selectedCategory == null || product.category == selectedCategory;
    };
    $scope.getCategoryClass = function (category) {
        return selectedCategory == category ? activeClass : "";
    };
    $scope.getPageClass = function (page) {
        return $scope.selectedPage == page ? activeClass : "";
    };

    $scope.addProductToCart = function (product) {
        cart.addProduct(product.id, product.name, product.price);
    };

});

app.controller('CartController',function ($scope, cart) {
    $scope.cartSummary = cart.getProducts();

    $scope.total = function(){
        var total = 0;
        angular.forEach($scope.cartSummary,function(item){
            total += (item.count * item.price)
        });
        return total
    };

    $scope.remove = function(id){
        cart.removeProduct(id)
    };
})

app.controller('OrderController',function($scope,$http, $location,cart){
    $scope.cartSummary = cart.getProducts();
    
    $scope.createOrder = function(cartSummary){
        var orderDetail = {};
        orderDetail.products = cartSummary;
        for(var key in $scope.order ){
            orderDetail[key] = $scope.order[key]
        }
        $http({
            method : "POST",
            url : 'http://localhost:2403/orders',
            data : orderDetail
        }).then(function(result){
            $scope.orderID = result.data.id;
            cart.getProducts().length = 0;
        }).catch(function(err){
            $scope.error = err
        }).finally(function(){
            $location.path('/complete')
        })
    }
})

app.filter("unique", function () {
    return function (data, propertyName) {
        if (angular.isArray(data) && angular.isString(propertyName)) {
            var results = [];
            var keys = {};
            for (var i = 0; i < data.length; i++) {
                var val = data[i][propertyName];
                if (angular.isUndefined(keys[val])) {
                    keys[val] = true;
                    results.push(val);
                }
            }
            return results;
        } else {
            return data;
        }
    }
});

app.filter("range", function ($filter) {
    return function (data, page, size) {
        if (angular.isArray(data) && angular.isNumber(page) && angular.isNumber(size)) {
            var start_index = (page - 1) * size;
            if (data.length < start_index) {
                return [];
            } else {
                return $filter("limitTo")(data.splice(start_index), size);
            }
        } else {
            return data;
        }
    }
});
app.filter("pageCount", function () {
    return function (data, size) {
        if (angular.isArray(data)) {
            var result = [];
            for (var i = 0; i < Math.ceil(data.length / size); i++) {
                result.push(i);
            }
            return result;
        } else {
            return data;
        }
    }
});



