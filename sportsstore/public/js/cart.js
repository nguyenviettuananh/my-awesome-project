var app = angular.module("cart",[]);

app.factory("cart",function () {
    var cartData = [];
    return {
        addProduct : function(id,name,price){
            var isItemExisting = false;
            for(var i = 0 ; i < cartData.length; i++){
                if(cartData[i].id == id){
                    cartData[i].count++;
                    isItemExisting = true;
                    break;
                }
            }
            if(isItemExisting == false){
                cartData.push({
                    count : 1,
                    id : id,
                    name : name,
                    price : price
                })
            }
        },

        removeProduct : function(id){
            for(var i = 0 ; i < cartData.length ; i++){
                if (cartData[i].id == id) {
                    cartData.splice(i, 1);
                    break;
                }
            }
        },

        getProducts : function(){
            return cartData;
        }
    }
});

app.directive("cartSummary",function(cart){
    return {
        restrict : 'E',
        templateUrl : '/view/cartSummary.html',
        controller : function($scope){
            var cartData = cart.getProducts();
            $scope.total = function() {
                var total = 0;
                for(var i = 0 ; i < cartData.length ; i++){
                    total +=  (cartData[i].count * cartData[i].price);
                }
                return total;
            };
            
            $scope.itemCount = function () {
                var total = 0;
                for (var i = 0; i < cartData.length; i++) {
                    total += cartData[i].count;
                }
                return total;
            }
        }
    }
})